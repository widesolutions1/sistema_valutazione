<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<html lang="en">
  <head>
    <!-- META SECTION -->
    <title>Dossier Studente</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="<c:url value="css/theme-default.css"/>">
    <!-- EOF CSS INCLUDE -->
  </head>
  <body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
      <!-- START PAGE SIDEBAR -->
      <div class="page-sidebar page-sidebar-fixed scroll">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
          <li class="xn-logo">
            <a href="dashboard.html">Widesolutions.it</a>
            <a href="#" class="x-navigation-control"></a>
          </li>
          <li class="xn-title">Navigation</li>
          <li>
            <a href="dashboard.html"><span class="fa fa-desktop"></span><span class="xn-text">Dashboard</span></a>
          </li>
          <li>
            <a href="studenti.html"><span class="fa fa-graduation-cap"></span><span class="xn-text">Studenti</span></a>
          </li>
          <li>
            <a href="dashboard.html"><span class="fa fa-male"></span><span class="xn-text">Professori</span></a>
          </li>
        </ul>
        <!-- END X-NAVIGATION -->
      </div>
      <!-- END PAGE SIDEBAR -->

      <!-- PAGE CONTENT -->
      <div class="page-content">
        <!-- START X-NAVIGATION VERTICAL -->
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
          <!-- TOGGLE NAVIGATION -->
          <li class="xn-icon-button">
            <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
          </li>
          <!-- END TOGGLE NAVIGATION -->
          <div class="x-features">
            <div class="pull-right">
              <img src="assets/images/users/avatar.jpg" style="
                  width: 40px;
                  border-radius: 50%;
                  margin-top: 5px;
                  margin-right: 5px;
                "/>
            </div>
            <div class="pull-right">
              <h3 class="color" style="margin: 15px">Nome Cognome</h3>
            </div>
          </div>
        </ul>
        <!-- END X-NAVIGATION VERTICAL -->

        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
          <li><a href="dashboard.html">Home</a></li>
          <li><a href="studenti.html">Studenti</a></li>
          <li class="active">Dossier </li>
        </ul>
        <!-- END BREADCRUMB -->
        <div class="page-title">
          <div class="row" style="margin-left: 7px;">
            <h2>
              <span class="fa fa-graduation-cap"></span> Dossier
            </h2>
          </div>
          <div class="row" style="margin-left: 7px;">
            <h3 style="color: #26A653; margin-bottom: 2%;">
              ISCRITTA DAL
            </h3>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="widget widget-success widget-item-icon" style="padding-left: 10% !important">
                <div class="widget-item-left">
                  <span class="fa fa-bookmark-o"></span>
                </div>
                <div class="widget-data">
                  <div class="widget-int num-count">4</div>
                  <div class="widget-title">Numero corsi iscritti</div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="widget widget-success widget-item-icon" style="padding-left: 10% !important">
                <div class="widget-item-left">
                  <span class="fa fa-star-half-o"></span>
                </div>
                <div class="widget-data">
                  <div class="widget-int num-count">230</div>
                  <div class="widget-title">Punti complessivi</div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="widget widget-success widget-item-icon" style="padding-left: 10% !important">
                <div class="widget-item-left">
                  <span class="fa fa-bar-chart-o"  style="margin-left: -20%;"></span>
                </div>
                <div class="widget-data">
                  <div class="widget-int num-count">8,5</div>
                  <div class="widget-title">Media voti</div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-4">
              <div class="widget widget-success widget-item-icon" style="padding-left: 10% !important">
                <div class="widget-item-left">
                  <span class="fa fa-code" style="margin-left: -20%;"></span>
                </div>
                <div class="widget-data">
                  <div class="widget-int num-count">15/16</div>
                  <div class="widget-title">Assignment Passati</div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="widget widget-success widget-item-icon" style="padding-left: 10% !important">
                <div class="widget-item-left">
                  <span class="fa fa-book"></span>
                </div>
                <div class="widget-data">
                  <div class="widget-int num-count">40/40</div>
                  <div class="widget-title">Lesson svolte/totale</div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="widget widget-success widget-item-icon" style="padding-left: 10% !important">
                <div class="widget-item-left">
                  <span class="fa fa-check-square-o"></span>
                </div>
                <div class="widget-data">
                  <div class="widget-int num-count">4/4</div>
                  <div class="widget-title">Quiz passati</div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <!-- PAGE CONTENT TABBED -->
            <div class="row">
              <div class="col-md-12" style="margin-top: 20px !important; margin-bottom: -20px !important;">
                <div class="page-tabs">
                  <a href="#first-tab" class="active">Assignment</a>
                  <a href="#second-tab">Quiz</a>
                  <a href="#third-tab">Interrogazioni</a>
                  <a href="#fourth-tab">Corsi/Lessons</a>
                </div>
              </div>
            </div>
            <div class="page-content-wrap page-tabs-item active" id="first-tab">
              <div class="row">
                <div class="col-md-12">
                  <!-- START DEFAULT DATATABLE -->
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table datatable">
                          <thead>
                            <tr>
                              <th>Corso</th>
                              <th>Titolo Assignment</th>
                              <th>Punteggio</th>
                              <th>Superato</th>
                              <th>Data</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Front-End Developer</td>
                              <td>Acme Photography</td>
                              <td>20</td>
                              <td>superato</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Back-End Developer</td>
                              <td>Circle Area</td>
                              <td>20</td>
                              <td>superato</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Data Scientist</td>
                              <td>Esempio</td>
                              <td>20</td>
                              <td>superato</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Database Administrator</td>
                              <td>Catalogo Viaggi Studio</td>
                              <td>20</td>
                              <td>superato</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Test</td>
                              <td>Esempio</td>
                              <td>20</td>
                              <td>non superato</td>
                              <td>21/11/2021</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <!-- END DEFAULT DATATABLE -->
                </div>
              </div>
            </div>
            <div class="page-content-wrap page-tabs-item" id="second-tab">
              <div class="row">
                <div class="col-md-12">
                  <!-- START DEFAULT DATATABLE -->
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table datatable">
                          <thead>
                            <tr>
                              <th>Corso</th>
                              <th>Titolo Quiz</th>
                              <th>Domande ok/totale</th>
                              <th>Superato</th>
                              <th>Data</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Front-End Developer</td>
                              <td>Javascript</td>
                              <td>20/20</td>
                              <td>superato</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Back-End Developer</td>
                              <td>Java - Fondamenti di Programmazione</td>
                              <td>20/20</td>
                              <td>superato</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Data Scientist</td>
                              <td>Esempio</td>
                              <td>20/20</td>
                              <td>superato</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Database Administrator</td>
                              <td>Google Form</td>
                              <td>20/20</td>
                              <td>superato</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Test</td>
                              <td>Esempio</td>
                              <td>20/20</td>
                              <td>non superato</td>
                              <td>21/11/2021</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <!-- END DEFAULT DATATABLE -->
                </div>
              </div>
            </div>
            <div class="page-content-wrap page-tabs-item" id="third-tab">
              <div class="row">
                <div class="col-md-12">
                  <!-- START DEFAULT DATATABLE -->
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table datatable">
                          <thead>
                            <tr>
                              <th>Corso</th>
                              <th>Titolo Interrogazione</th>
                              <th>Professore</th>
                              <th>Voto Assegnato</th>
                              <th>Data</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Front-End Developer</td>
                              <td>Javascript</td>
                              <td>Alessandro Gobbo</td>
                              <td>10/10</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Back-End Developer</td>
                              <td>Java</td>
                              <td>Francesco Festa</td>
                              <td>10/10</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Data Scientist</td>
                              <td>Reti Neurali</td>
                              <td>Costantino Palladino</td>
                              <td>10/10</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Database Administrator</td>
                              <td>Entità</td>
                              <td>Costantino Palladino</td>
                              <td>10/10</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Test</td>
                              <td>Esempio</td>
                              <td>Dante Alighieri</td>
                              <td>10/10</td>
                              <td>21/11/2021</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <!-- END DEFAULT DATATABLE -->
                </div>
              </div>
            </div>
            <div class="page-content-wrap page-tabs-item" id="fourth-tab">
              <div class="row">
                <div class="col-md-12">
                  <!-- START DEFAULT DATATABLE -->
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <div class="table-responsive">
                        <table class="table datatable">
                          <thead>
                            <tr>
                              <th>Corso</th>
                              <th>Titolo Lesson</th>
                              <th>Data</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Front-End Developer</td>
                              <td>JS - Next Gen</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Back-End Developer</td>
                              <td>Classi, Interfacce, Classi Astratte</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Data Scientist</td>
                              <td>Calcolo Combinatorio</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Database Administrator</td>
                              <td>Procedure</td>
                              <td>21/11/2021</td>
                            </tr>
                            <tr>
                              <td>Test</td>
                              <td>Esempio</td>
                              <td>21/11/2021</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <!-- END DEFAULT DATATABLE -->
                </div>
              </div>
            </div>
            <!-- END PAGE CONTENT TABBED -->
          </div>
        </div>

      </div>
      <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
      <div class="mb-container">
        <div class="mb-middle">
          <div class="mb-title">
            <span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?
          </div>
          <div class="mb-content">
            <p>Are you sure you want to log out?</p>
            <p>
              Press No if youwant to continue work. Press Yes to logout current
              user.
            </p>
          </div>
          <div class="mb-footer">
            <div class="pull-right">
              <a href="pages-login.html" class="btn btn-success btn-lg">Yes</a>
              <button class="btn btn-default btn-lg mb-control-close">
                No
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END MESSAGE BOX-->


    <!-- START SCRIPTS -->
    <!-- START PLUGINS -->
    <script type="text/javascript" src="<c:url value="js/plugins/jquery/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="js/plugins/jquery/jquery-ui.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="js/plugins/bootstrap/bootstrap.min.js"/>"></script>
    <!-- END PLUGINS -->

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src='<c:url value="js/plugins/icheck/icheck.min.js"/>'></script>
    <script type="text/javascript" src="<c:url value="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="js/plugins/datatables/jquery.dataTables.min.js"/>"></script>
    <!-- END PAGE PLUGINS -->

    <!-- START TEMPLATE -->
    <script type="text/javascript" src="<c:url value="js/plugins.js"/>"></script>
    <script type="text/javascript" src="<c:url value="js/actions.js"/>"></script>
    <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->
  </body>
</html>
