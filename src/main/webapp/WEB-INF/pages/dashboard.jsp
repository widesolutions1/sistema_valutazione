<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- META SECTION -->
    <title>Dashboard</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link
      rel="stylesheet"
      type="text/css"
      id="theme"
      href="css/theme-default.css"
    />
    <!-- EOF CSS INCLUDE -->
  </head>
  <body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
      <!-- START PAGE SIDEBAR -->
      <div class="page-sidebar page-sidebar-fixed scroll">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
          <li class="xn-logo">
            <a href="dashboard.html">Widesolutions.it</a>
            <a href="#" class="x-navigation-control"></a>
          </li>
          <li class="xn-title">Navigation</li>
          <li>
            <a href="dashboard.html"
              ><span class="fa fa-desktop"></span>
              <span class="xn-text">Dashboard</span></a
            >
          </li>
          <li>
            <a href="studenti.html"
              ><span class="fa fa-graduation-cap"></span>
              <span class="xn-text">Studenti</span></a
            >
          </li>
          <li>
            <a href="dashboard.html"
              ><span class="fa fa-male"></span>
              <span class="xn-text">Professori</span></a
            >
          </li>
        </ul>
        <!-- END X-NAVIGATION -->
      </div>
      <!-- END PAGE SIDEBAR -->

      <!-- PAGE CONTENT -->
      <div class="page-content">
        <!-- START X-NAVIGATION VERTICAL -->
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
          <!-- TOGGLE NAVIGATION -->
          <li class="xn-icon-button">
            <a href="#" class="x-navigation-minimize"
              ><span class="fa fa-dedent"></span
            ></a>
          </li>
          <!-- END TOGGLE NAVIGATION -->
          <div class="x-features">
            <div class="pull-right">
              <img
                src="assets/images/users/avatar.jpg"
                style="width: 40px; border-radius: 50%; margin-top: 5px; margin-right: 5px;"
              />
            </div>
            <div class="pull-right">
              <h3 class="color" style="margin: 15px">Nome Cognome</h3>
            </div>
          </div>
        </ul>
        <!-- END X-NAVIGATION VERTICAL -->

        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
          <li class="active">Home</li>
        </ul>
        <!-- END BREADCRUMB -->

        <div class="page-title">
          <h2><span class="fa fa-desktop"></span> Dashboard</h2>
        </div>

        <!-- PAGE CONTENT WRAPPER -->
        <div class="page-content-wrap">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                
              </div>
            </div>
          </div>
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
      </div>
      <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
      <div class="mb-container">
        <div class="mb-middle">
          <div class="mb-title">
            <span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?
          </div>
          <div class="mb-content">
            <p>Are you sure you want to log out?</p>
            <p>
              Press No if youwant to continue work. Press Yes to logout current
              user.
            </p>
          </div>
          <div class="mb-footer">
            <div class="pull-right">
              <a href="pages-login.html" class="btn btn-success btn-lg">Yes</a>
              <button class="btn btn-default btn-lg mb-control-close">
                No
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END MESSAGE BOX-->


    <!-- START SCRIPTS -->
    <!-- START PLUGINS -->
    <script
      type="text/javascript"
      src="js/plugins/jquery/jquery.min.js"
    ></script>
    <script
      type="text/javascript"
      src="js/plugins/jquery/jquery-ui.min.js"
    ></script>
    <script
      type="text/javascript"
      src="js/plugins/bootstrap/bootstrap.min.js"
    ></script>
    <!-- END PLUGINS -->

    <!-- THIS PAGE PLUGINS -->

    <!-- END PAGE PLUGINS -->

    <!-- START TEMPLATE -->
    <script type="text/javascript" src="js/plugins.js"></script>
    <script type="text/javascript" src="js/actions.js"></script>
    <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->
  </body>
</html>
