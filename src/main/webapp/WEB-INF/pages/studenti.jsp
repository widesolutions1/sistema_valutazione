<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<html lang="en">
  <head>
    <!-- META SECTION -->
    <title>Studenti</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="<c:url value="css/theme-default.css"/>">
    <!-- EOF CSS INCLUDE -->
  </head>
  <body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">
      <!-- START PAGE SIDEBAR -->
      <div class="page-sidebar page-sidebar-fixed scroll">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
          <li class="xn-logo">
            <a href="dashboard.html">Widesolutions.it</a>
            <a href="#" class="x-navigation-control"></a>
          </li>
          <li class="xn-title">Navigation</li>
          <li>
            <a href="dashboard.html"
              ><span class="fa fa-desktop"></span>
              <span class="xn-text">Dashboard</span></a
            >
          </li>
          <li>
            <a href="studenti.html"
              ><span class="fa fa-graduation-cap"></span>
              <span class="xn-text">Studenti</span></a
            >
          </li>
          <li>
            <a href="dashboard.html"
              ><span class="fa fa-male"></span>
              <span class="xn-text">Professori</span></a
            >
          </li>
        </ul>
        <!-- END X-NAVIGATION -->
      </div>
      <!-- END PAGE SIDEBAR -->

      <!-- PAGE CONTENT -->
      <div class="page-content">
        <!-- START X-NAVIGATION VERTICAL -->
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
          <!-- TOGGLE NAVIGATION -->
          <li class="xn-icon-button">
            <a href="#" class="x-navigation-minimize"
              ><span class="fa fa-dedent"></span
            ></a>
          </li>
          <!-- END TOGGLE NAVIGATION -->
          <div class="x-features">
            <div class="pull-right">
              <img
                src="assets/images/users/avatar.jpg"
                style="width: 40px; border-radius: 50%; margin-top: 5px; margin-right: 5px;"
              />
            </div>
            <div class="pull-right">
              <h3 class="color" style="margin: 15px">Nome Cognome</h3>
            </div>
          </div>
        </ul>
        <!-- END X-NAVIGATION VERTICAL -->

        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
          <li><a href="dashboard.html">Home</a></li>
          <li class="active">Studenti</li>
        </ul>
        <!-- END BREADCRUMB -->

        <div class="page-title">
          <h2><span class="fa fa-graduation-cap"></span> Studenti</h2>
        </div>

        <!-- PAGE CONTENT WRAPPER -->
        <div class="page-content-wrap">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                
                <div class="panel-body">
                  <!-- START DEFAULT DATATABLE -->
                  
                      <div class="table-responsive">
                        <table class="table datatable">
                          <thead>
                            <tr>
                              <th>Nome / Cognome</th> <!--user_login-->
                              <th>Data Iscrizione</th> <!--user_registered-->
                              <th>N° Corsi Iscritti</th> <!-- calcolo su tabella userxcourse in base agli id degli user-->
                              <th>Punti Complessivi</th> <!-- prendi da user_points(vista)-->
                              <th>Media Voti</th> <!-- ???????????????????????????? -->
                              <th>Assignment Passati</th> <!-- calcolo filtra per 'assignment_passed'/action_id-->
                              <th>Lesson Svolte / Totale</th> <!-- calcolo filtra per 'lesson_passed'/action_id x completed = 1-->
                              <th>Quiz Passati</th> <!-- calcolo filtra per 'quiz_passed'/action_id x completed = 1-->
                            </tr>
                          </thead>
                          <tbody>
                          <c:forEach var="ws_student" items="${ws_students}">
                            <tr>
                              <td>${ws_student.student_login}</td>
                              <td>${ws_student.student_registered}</td>
                              <td> </td>
                              <td> </td>
                              <td> </td>
                              <td> </td>
                              <td> </td>
                              <td> </td>
                            </tr>
                          </c:forEach>
                          </tbody>
                        </table>
                      </div>
                    
                  <!-- END DEFAULT DATATABLE -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
      </div>
      <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
      <div class="mb-container">
        <div class="mb-middle">
          <div class="mb-title">
            <span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?
          </div>
          <div class="mb-content">
            <p>Are you sure you want to log out?</p>
            <p>
              Press No if youwant to continue work. Press Yes to logout current
              user.
            </p>
          </div>
          <div class="mb-footer">
            <div class="pull-right">
              <a href="pages-login.html" class="btn btn-success btn-lg">Yes</a>
              <button class="btn btn-default btn-lg mb-control-close">
                No
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END MESSAGE BOX-->

    <!-- START SCRIPTS -->
    <!-- START PLUGINS -->
    <script type="text/javascript" src="<c:url value="js/plugins/jquery/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="js/plugins/jquery/jquery-ui.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="js/plugins/bootstrap/bootstrap.min.js"/>"></script>
    <!-- END PLUGINS -->

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src='<c:url value="js/plugins/icheck/icheck.min.js"/>'></script>
    <script type="text/javascript" src="<c:url value="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="js/plugins/datatables/jquery.dataTables.min.js"/>"></script>
    <!-- END PAGE PLUGINS -->

    <!-- START TEMPLATE -->
    <script type="text/javascript" src="<c:url value="js/plugins.js"/>"></script>
    <script type="text/javascript" src="<c:url value="js/actions.js"/>"></script>
    <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->
  </body>
</html>
