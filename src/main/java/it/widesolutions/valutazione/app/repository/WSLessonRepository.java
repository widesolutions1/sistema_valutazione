package it.widesolutions.valutazione.app.repository;

import it.widesolutions.valutazione.app.entity.revision.WSLesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WSLessonRepository extends JpaRepository<WSLesson,Integer> {
}
