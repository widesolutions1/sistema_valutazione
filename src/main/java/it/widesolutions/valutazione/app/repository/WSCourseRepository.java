package it.widesolutions.valutazione.app.repository;

import it.widesolutions.valutazione.app.entity.revision.WSCourse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WSCourseRepository extends JpaRepository<WSCourse,Integer> {
}
