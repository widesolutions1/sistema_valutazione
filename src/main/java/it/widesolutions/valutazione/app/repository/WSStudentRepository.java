package it.widesolutions.valutazione.app.repository;

import it.widesolutions.valutazione.app.entity.revision.WSInstructor;
import it.widesolutions.valutazione.app.entity.revision.WSStudent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WSStudentRepository extends JpaRepository<WSStudent, Integer> {

}
