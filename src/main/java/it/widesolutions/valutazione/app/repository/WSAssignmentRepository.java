package it.widesolutions.valutazione.app.repository;

import it.widesolutions.valutazione.app.entity.revision.WSAssignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WSAssignmentRepository extends JpaRepository<WSAssignment,Integer> {
}
