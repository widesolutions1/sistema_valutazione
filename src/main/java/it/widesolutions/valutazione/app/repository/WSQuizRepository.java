package it.widesolutions.valutazione.app.repository;

import it.widesolutions.valutazione.app.entity.revision.WSQuiz;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WSQuizRepository extends JpaRepository<WSQuiz, Integer> {




}
