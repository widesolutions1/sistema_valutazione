package it.widesolutions.valutazione.app.service;

import it.widesolutions.valutazione.app.entity.revision.WSStudent;

import java.util.List;

public interface WSStudentService {

	public List<WSStudent> findAll(); //devo ritornare il dto
	public WSStudent findById(int id);

}
