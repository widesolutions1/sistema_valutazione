package it.widesolutions.valutazione.app.service;


import it.widesolutions.valutazione.app.entity.revision.WSAssignment;

import java.util.List;

public interface WSAssignmentService {
    public List<WSAssignment> findAll();
    public WSAssignment findById(int id);
}
