package it.widesolutions.valutazione.app.service;

import it.widesolutions.valutazione.app.entity.revision.WSLesson;
import it.widesolutions.valutazione.app.repository.WSLessonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WSLessonServiceImpl implements WSLessonService {

    private final WSLessonRepository wpLessonRepository;

    @Override
    public List<WSLesson> findAll() {
        return wpLessonRepository.findAll();
    }

    @Override
    public WSLesson findById(int id) {
        WSLesson wsLesson = null;
        Optional<WSLesson> result = wpLessonRepository.findById(id);

        if(result.isPresent()){
            wsLesson = result.get();
        } else throw new RuntimeException("This lesson id doesn't match with an existing lesson: "+ id);

        return wsLesson;
    }
}
