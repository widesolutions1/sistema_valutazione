package it.widesolutions.valutazione.app.service;


import it.widesolutions.valutazione.app.entity.revision.WSLesson;

import java.util.List;

public interface WSLessonService {
    public List<WSLesson> findAll();
    public WSLesson findById(int id);
}
