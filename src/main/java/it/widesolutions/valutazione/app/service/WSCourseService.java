package it.widesolutions.valutazione.app.service;


import it.widesolutions.valutazione.app.entity.revision.WSCourse;

import java.util.List;

public interface WSCourseService {
    public List<WSCourse> findAll();
    public WSCourse findById(int id);
}
