package it.widesolutions.valutazione.app.service;

import it.widesolutions.valutazione.app.entity.revision.WSInstructor;
import it.widesolutions.valutazione.app.entity.revision.WSStudent;

import java.util.List;

public interface WSInstructorService {

	public List<WSInstructor> findAll(); //devo ritornare il dto
	public WSInstructor findById(int id);

}
