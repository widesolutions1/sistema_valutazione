package it.widesolutions.valutazione.app.service;

import it.widesolutions.valutazione.app.entity.revision.WSQuiz;

import java.util.List;

public interface WSQuizService {

	public List<WSQuiz> findAll();
	
	public WSQuiz findById(int id);

}
