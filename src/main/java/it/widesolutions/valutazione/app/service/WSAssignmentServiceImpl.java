package it.widesolutions.valutazione.app.service;

import it.widesolutions.valutazione.app.entity.revision.WSAssignment;
import it.widesolutions.valutazione.app.repository.WSAssignmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WSAssignmentServiceImpl implements WSAssignmentService {

    private final WSAssignmentRepository wpAssignmentRepository;

    @Override
    public List<WSAssignment> findAll() {
        return wpAssignmentRepository.findAll();
    }

    @Override
    public WSAssignment findById(int id) {
        WSAssignment wsAssignment = null;
        Optional<WSAssignment> result = wpAssignmentRepository.findById(id);

        if(result.isPresent()){
            wsAssignment = result.get();
        } else throw new RuntimeException("This assignment id doesn't match with an existing assignment: "+ id);

        return wsAssignment;
    }
}
