package it.widesolutions.valutazione.app.service;

import it.widesolutions.valutazione.app.entity.revision.WSQuiz;
import it.widesolutions.valutazione.app.repository.WSQuizRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WSQuizServiceImpl implements WSQuizService {

	private final WSQuizRepository wpQuizRepository;

	@Override
	public List<WSQuiz> findAll() {
		return wpQuizRepository.findAll();
	}

	@Override
	public WSQuiz findById(int id) {
		Optional<WSQuiz> result = wpQuizRepository.findById(id);

		WSQuiz wsQuiz = null;
		
		if (result.isPresent()) {
			wsQuiz = result.get();
		}
		else {
			throw new RuntimeException("Did not find quiz id - " + id);
		}
		
		return wsQuiz;
	}

}






