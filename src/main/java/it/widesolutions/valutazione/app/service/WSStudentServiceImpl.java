package it.widesolutions.valutazione.app.service;

import it.widesolutions.valutazione.app.entity.revision.WSStudent;
import it.widesolutions.valutazione.app.repository.WSStudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WSStudentServiceImpl implements WSStudentService {

	private final WSStudentRepository wsStudentRepository;

	@Override
	public List<WSStudent> findAll() {
		return wsStudentRepository.findAll();
	}

	@Override
	public WSStudent findById(int id) {
		Optional<WSStudent> result = wsStudentRepository.findById(id);
		WSStudent wsStudent = null;
		
		if (result.isPresent()) {
			wsStudent = result.get();
		} else {
			throw new RuntimeException("Did not find user with id " + id);
		}
		
		return wsStudent;
	}


}






