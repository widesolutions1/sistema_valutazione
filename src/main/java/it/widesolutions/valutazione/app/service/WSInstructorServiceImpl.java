package it.widesolutions.valutazione.app.service;

import it.widesolutions.valutazione.app.entity.revision.WSInstructor;
import it.widesolutions.valutazione.app.repository.WSInstructorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WSInstructorServiceImpl implements WSInstructorService {

	private final WSInstructorRepository wsInstructorRepository;

	@Override
	public List<WSInstructor> findAll() {
		return wsInstructorRepository.findAll();
	}

	@Override
	public WSInstructor findById(int id) {
		Optional<WSInstructor> result = wsInstructorRepository.findById(id);
		WSInstructor wsInstructor = null;
		
		if (result.isPresent()) {
			wsInstructor = result.get();
		} else {
			throw new RuntimeException("Did not find user with id " + id);
		}
		
		return wsInstructor;
	}


}






