package it.widesolutions.valutazione.app.service;

import it.widesolutions.valutazione.app.entity.revision.WSCourse;
import it.widesolutions.valutazione.app.repository.WSCourseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WSCourseServiceImpl implements WSCourseService {

    private final WSCourseRepository wsCourseRepository;

    @Override
    public List<WSCourse> findAll() {
        return wsCourseRepository.findAll();
    }

    @Override
    public WSCourse findById(int id) {
        WSCourse wsCourse = null;
        Optional<WSCourse> result = wsCourseRepository.findById(id);

        if(result.isPresent()){
            wsCourse = result.get();
        } else throw new RuntimeException("This course id doesn't match with an existing course: "+ id);

        return wsCourse;
    }
}
