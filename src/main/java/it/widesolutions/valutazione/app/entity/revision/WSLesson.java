package it.widesolutions.valutazione.app.entity.revision;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name="ws_lessons")
@NamedNativeQuery(name = "findlessons", query = "select * from ws_lessons", resultClass = WSLesson.class)
public class WSLesson {
    
    @Id
    @Column(name = "lesson_id")
    private Integer lesson_id;

    @Column(name = "lesson_date")
    private String lesson_date;
    
    @Column(name = "lesson_content")
    private String lesson_content;

    @Column(name = "lesson_title")
    private String lesson_title;

    @Column(name = "lesson_name")
    private String lesson_name;


}
