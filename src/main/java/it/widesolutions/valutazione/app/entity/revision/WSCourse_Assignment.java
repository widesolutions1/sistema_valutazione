package it.widesolutions.valutazione.app.entity.revision;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name="ws_course_assignments")
@NamedNativeQuery(name = "findCourseAssingnments", query = "select * from ws_course_assignments", resultClass = WSCourse_Assignment.class)
public class WSCourse_Assignment implements Serializable {

    @Id
    @Column(name = "course_id")
    private Integer course_id;

    @Id
    @Column(name = "assignment_id")
    private Integer assignment_id;

    @Column(name = "item_type")
    private String item_type;

}
