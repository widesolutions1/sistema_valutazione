package it.widesolutions.valutazione.app.entity.revision;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name="ws_quizzes")
@NamedNativeQuery(name = "findQuizzes", query = "select * from ws_quizzes", resultClass = WSQuiz.class)
public class WSQuiz {
    @Id
    @Column(name = "quiz_id")
    private Integer quiz_id;

    @Column(name = "quiz_date")
    private String quiz_date;

    @Column(name = "quiz_content")
    private String quiz_content;

    @Column(name = "quiz_title")
    private String quiz_title;

    @Column(name = "quiz_name")
    private String quiz_name;


}

