package it.widesolutions.valutazione.app.entity.revision;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name="ws_course_quizzes")
@NamedNativeQuery(name = "findCourseQuizzes", query = "select * from ws_course_quizzes", resultClass = WSCourse_Quiz.class)
public class WSCourse_Quiz implements Serializable {

    @Id
    @Column(name = "course_id")
    private Integer course_id;

    @Id
    @Column(name = "quiz_id")
    private Integer quiz_id;

    @Column(name = "item_type")
    private String item_type;


}
