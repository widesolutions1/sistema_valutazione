package it.widesolutions.valutazione.app.entity.revision;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name="ws_assignments")
@NamedNativeQuery(name = "findAssignments", query = "select * from ws_assignments", resultClass = WSAssignment.class)
public class WSAssignment {
    
    @Id
    @Column(name = "assignment_id")
    private Integer assignment_id;

    @Column(name = "assignment_date")
    private String assignment_date;
    
    @Column(name = "assignment_content")
    private String assignment_content;

    @Column(name = "assignment_title")
    private String assignment_title;

    @Column(name = "assignment_name")
    private String assignment_name;

}
