package it.widesolutions.valutazione.app.entity.revision;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name="ws_user_courses")
@NamedNativeQuery(name = "findUserCourses", query = "select * from ws_user_courses", resultClass = WSUser_Courses.class)
public class WSUser_Courses implements Serializable {

    @Id
    @Column(name = "course_id")
    private Integer course_id;

    @Id
    @Column(name = "user_id")
    private Integer user_id;

    @Column(name = "item_type")
    private String item_type;


}
