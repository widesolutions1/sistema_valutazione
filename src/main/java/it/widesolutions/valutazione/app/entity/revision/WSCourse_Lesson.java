package it.widesolutions.valutazione.app.entity.revision;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name="ws_course_lessons")
@NamedNativeQuery(name = "findCourseLessons", query = "select * from ws_course_lessons", resultClass = WSCourse_Lesson.class)
public class WSCourse_Lesson implements Serializable {

    @Id
    @Column(name = "course_id")
    private Integer course_id;

    @Id
    @Column(name = "lesson_id")
    private Integer lesson_id;

    @Column(name = "item_type")
    private String item_type;


}
