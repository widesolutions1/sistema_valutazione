package it.widesolutions.valutazione.app.entity.revision;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name="ws_student")
@NamedNativeQuery(name = "findStudent", query = "select * from ws_student", resultClass = WSStudent.class)
public class WSStudent {
    
        @Id
        @GeneratedValue(strategy= GenerationType.IDENTITY)
        @Column(name = "user_id")
        private Integer student_id;

        @Column(name = "user_login")
        private String student_login;

        @Column(name = "user_nicename")
        private String student_nicename;

        @Column(name = "user_email")
        private String student_email;

        @Column(name = "registration_date")
        private String student_registered;

        @Column(name = "display_name")
        private String student_name;

        @Column(name = "user_role")
        private String student_role;

}
