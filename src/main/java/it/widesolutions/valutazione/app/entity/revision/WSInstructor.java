package it.widesolutions.valutazione.app.entity.revision;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(name="ws_instructor")
@NamedNativeQuery(name = "findInstructor", query = "select * from ws_instructor", resultClass = WSInstructor.class)
public class WSInstructor {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer instructor_id;

    @Column(name = "user_login")
    private String instructor_login;

    @Column(name = "user_nicename")
    private String instructor_nicename;

    @Column(name = "user_email")
    private String instructor_email;

    @Column(name = "registration_date")
    private String instructor_registered;

    @Column(name = "display_name")
    private String instructor_name;

    @Column(name = "user_role")
    private String instructor_role;

}
