package it.widesolutions.valutazione.app.entity.revision;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name="ws_courses")
@NamedNativeQuery(name = "findCourses", query = "select * from ws_courses", resultClass = WSCourse.class)
public class WSCourse {
    
    @Id
    @Column(name = "course_id")
    private Integer course_id;

    @Column(name = "course_date")
    private String course_date;
    
    @Column(name = "course_content")
    private String course_content;

    @Column(name = "course_title")
    private String course_title;

    @Column(name = "course_name")
    private String course_name;



}
