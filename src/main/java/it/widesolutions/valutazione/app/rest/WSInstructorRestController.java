package it.widesolutions.valutazione.app.rest;

import it.widesolutions.valutazione.app.entity.revision.WSInstructor;
import it.widesolutions.valutazione.app.service.WSInstructorService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WSInstructorRestController {

    private static final Logger log = Logger.getLogger(WSInstructorRestController.class);
    private WSInstructorService wsInstructorService;

    @Autowired
    public WSInstructorRestController(WSInstructorService wsInstructorService) {
        this.wsInstructorService = wsInstructorService;
    }

/*
    @GetMapping("/get")
    public List<WSInstructor> getWSInstructors() {
        return WSInstructorService.findAll();
    }
*/

    @RequestMapping("/ws_instructors")
    public String getWSInstructors(Model model) {
        log.info("called path /ws_instructor");
        model.addAttribute("ws_instructors", wsInstructorService.findAll());
        return "pages/professori";
    }
    
    @RequestMapping("/instructor{id}")
    public String getWSInstructor(Model model, @PathVariable int id) {
        log.info("called path /wp_instructor/get/{id}");
        WSInstructor wsInstructor = wsInstructorService.findById(id);
        model.addAttribute("ws_instructor",wsInstructor);
        if (wsInstructor == null) {
            throw new RuntimeException("instructor not found: id  - " + id);
        }
        return "pages/dettaglio";
    }

}










