package it.widesolutions.valutazione.app.rest;

import it.widesolutions.valutazione.app.entity.revision.WSAssignment;
import it.widesolutions.valutazione.app.service.WSAssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/wp_assignment")
public class WPAssignmentRestController {

    private WSAssignmentService service;

    @Autowired
    public WPAssignmentRestController(WSAssignmentService WSAssignmentService) {
        service = WSAssignmentService;
    }

    @GetMapping("/get")
    private List<WSAssignment> getAssignment() {
        return service.findAll();
    }

    @GetMapping("/get/{id}")
    private WSAssignment getAssignment(@PathVariable int id) {
        WSAssignment assignment = service.findById(id);

        if (assignment == null) {
            throw new RuntimeException("Error : this assignment doesn't exist, check the id again. ID given: " + id);
        }
        return assignment;
    }
}
