package it.widesolutions.valutazione.app.rest;

import it.widesolutions.valutazione.app.entity.revision.WSStudent;
import it.widesolutions.valutazione.app.service.WSStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.log4j.Logger;

@Controller
public class WSStudentRestController {

    private static final Logger log = Logger.getLogger(WSStudentRestController.class);
    private WSStudentService wsStudentService;

    @Autowired
    public WSStudentRestController(WSStudentService wsStudentService) {
        this.wsStudentService = wsStudentService;
    }

    @RequestMapping("/ws_students")
    public String getWSStudents(Model model) {
        log.info("called path /ws_student");
        model.addAttribute("ws_students", wsStudentService.findAll());
        return "pages/studenti";
    }


    @RequestMapping("/student{id}")
    public String getWSStudent(Model model, @PathVariable int id) {
        log.info("called path /wp_user/get/{id}");
        WSStudent wsStudent = wsStudentService.findById(id);
        model.addAttribute("wp_user",wsStudent);
        if (wsStudent == null) {
            throw new RuntimeException("User not found: id  - " + id);
        }
        return "pages/dettaglio";
    }

}










