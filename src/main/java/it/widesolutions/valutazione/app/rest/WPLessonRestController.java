package it.widesolutions.valutazione.app.rest;

import it.widesolutions.valutazione.app.entity.revision.WSLesson;
import it.widesolutions.valutazione.app.service.WSLessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/wp_lesson")
public class WPLessonRestController {

    private WSLessonService service;

    @Autowired
    public WPLessonRestController(WSLessonService WSLessonService) {
        service = WSLessonService;
    }

    @GetMapping("/get")
    private List<WSLesson> getLesson() {
        return service.findAll();
    }

    @GetMapping("/get/{id}")
    private WSLesson getLesson(@PathVariable int id) {
        WSLesson lesson = service.findById(id);

        if (lesson == null) {
            throw new RuntimeException("Error : this lesson doesn't exist, check the id again. ID given: " + id);
        }
        return lesson;
    }
}
