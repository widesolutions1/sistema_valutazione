package it.widesolutions.valutazione.app.rest;

import it.widesolutions.valutazione.app.entity.revision.WSCourse;
import it.widesolutions.valutazione.app.service.WSCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/wp_course")
public class WPCourseRestController {

    private WSCourseService service;

    @Autowired
    public WPCourseRestController(WSCourseService wpcourseService) {
        service = wpcourseService;
    }

    @GetMapping("/get")
    private List<WSCourse> getCourse() {
        return service.findAll();
    }

    @GetMapping("/get/{id}")
    private WSCourse getcourse(@PathVariable int id) {
        WSCourse course = service.findById(id);

        if (course == null) {
            throw new RuntimeException("Error : this course doesn't exist, check the id again. ID given: " + id);
        }
        return course;
    }
}
