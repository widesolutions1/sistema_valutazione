package it.widesolutions.valutazione.app.rest;

import it.widesolutions.valutazione.app.entity.revision.WSQuiz;
import it.widesolutions.valutazione.app.service.WSQuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/wp_quizzes")
public class WPQuizRestController {


    private WSQuizService WSQuizService;

    @Autowired
    public WPQuizRestController(WSQuizService WSQuizService) {
        this.WSQuizService = WSQuizService;
    }

    @GetMapping("/get")
    public List<WSQuiz> findAll() {
        return WSQuizService.findAll();
    }

    @GetMapping("/get/{id}")
    public WSQuiz getQuiz(@PathVariable int id) {
        WSQuiz WSQuiz = WSQuizService.findById(id);
        if (WSQuiz == null) {
            throw new RuntimeException("Quiz with this id not found - " + id);
        }
        return WSQuiz;
    }

}










